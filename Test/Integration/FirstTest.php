<?php

namespace Assaka\Testing\Test\Integration;

use Magento\Framework\Component\ComponentRegistrar;
use Magento\Framework\Module\ModuleList;
use Magento\TestFramework\ObjectManager;
use Magento\Framework\Module\Output;
use Magento\Framework\App\Route\ConfigInterface;
use Magento\TestFramework\Request;


class FirstTest extends \PHPUnit_Framework_TestCase
{
    protected $moduleName= "Assaka_Testing";
    private $objectManager;
    protected $_moduleDir;
    protected $_registrar;
    protected $_outputConfig;

    /* @var  \Magento\Framework\Module\Manager */
    protected $_moduleManager;

//    public function testNothing()
//    {
//        $this->fail(':-)');
//
//    }

    protected function setUp()
    {
        $this->objectManager = ObjectManager::getInstance();
        $this->_registrar = new ComponentRegistrar();
        $objectManager = new \Magento\Framework\TestFramework\Unit\Helper\ObjectManager($this);
        $this->_moduleDir = $objectManager->getObject('\Magento\Framework\Module\Dir', [
            'componentRegistrar' => $this->_registrar
        ]);

        $this->_moduleManager = $objectManager->getObject("Magento\Framework\Module\Manager");

//        $realObjectManager = \Magento\TestFramework\Helper\Bootstrap::getObjectManager();
//        $this->_moduleManager = $realObjectManager->get("Magento\Framework\Module\Manager");

    }

    public function testRegistrationFileExists() {

        $this->assertFileExists($this->_moduleDir->getDir($this->moduleName) . DIRECTORY_SEPARATOR . 'registration.php');
        $_fileObj = new \SplFileObject($this->_moduleDir->getDir($this->moduleName) . DIRECTORY_SEPARATOR . 'registration.php');
        $this->assertEquals(true, $_fileObj->isReadable());

    }

    public function testModuleIsEnabled()
    {

//        if the module is enabled then it will show in the modulelist, but not shown whether it's output enabled or not
        /** @var ModuleList $moduleList */
        $moduleList = $this->objectManager->create(ModuleList::class);

//        var_dump($moduleList->getOne($this->moduleName));

        $message = sprintf('The module "%s" is not enabled', $this->moduleName);
        $this->assertTrue($moduleList->has($this->moduleName), $message);
    }

    public function testModulePathExist() {

//        get the directories of all the modules
//        var_dump($this->_registrar->getPaths(ComponentRegistrar::MODULE));

        $this->assertArrayHasKey($this->moduleName, $this->_registrar->getPaths(ComponentRegistrar::MODULE));

    }

    public function testOutputEnabled()
    {

//        check if the output of the module is enabled in the configuration
        $isOutputEnabled = $this->_moduleManager->isOutputEnabled($this->moduleName);

        $message = sprintf('The module output "%s" is not enabled', $this->moduleName);
        $this->assertTrue($isOutputEnabled, $message);

    }

//    public function testRouteIsConfigured()
//    {
//
//    $routeConfig = $this->objectManager->create(ConfigInterface::class);
//
//    var_dump($routeConfig->getRouteByFrontName('assakatest'));
//
//    $this->assertContains('Assaka_Testing', $routeConfig->getModulesByFrontName('assakatest'));
//    }

//    public function testActionControllerIsFound()
//    {
//        $request = $this->objectManager->create(Request::class);
//        $request->setModuleName('assaka');
//        $request->setControllerName('hello');
//        $request->setActionName('world');
//        /** @var BaseRouter $baseRouter */
//        $baseRouter = $this->objectManager->create(BaseRouter::class);
//        $expectedAction = \Assaka\Testing\Controller\Hello\World::class;
//        $this->assertInstanceOf($expectedAction, $baseRouter->match($request));
//    }

}