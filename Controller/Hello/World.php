<?php
namespace Assaka\Testing\Controller\Hello;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\View\Result\PageFactory;

class World extends \Magento\Framework\App\Action\Action
{
    protected $resultPageFactory;
    protected $_pageFactory;
    protected $_dataHelper;

    public function __construct(
        Context $context,
        PageFactory $pageFactory
//        Data $dataHelper
    ) {
        parent::__construct($context);
        $this->_pageFactory = $pageFactory;
//        $this->_dataHelper = $dataHelper;
    }

    public function execute()
    {

        $pageFactory = $this->_pageFactory->create();

        // Add title which is got by the configuration via backend
//        $pageFactory->getConfig()->getTitle()->set(
//            $this->_dataHelper->getHeadTitle()
//        );

        // Add breadcrumb
        /** @var \Magento\Theme\Block\Html\Breadcrumbs */
        $breadcrumbs = $pageFactory->getLayout()->getBlock('breadcrumbs');
        $breadcrumbs->addCrumb('home',
            [
                'label' => __('Home'),
                'title' => __('Home'),
                'link' => $this->_url->getUrl('')
            ]
        );
        $breadcrumbs->addCrumb('helloworld',
            [
                'label' => __('Hello world'),
                'title' => __('Hello world')
            ]
        );

        $this->_view->loadLayout();
        $this->_view->renderLayout();

    }
}