<?php

namespace Assaka\Testing\Block;

class Hello extends \Magento\Framework\View\Element\Template
{

    protected function _prepareLayout()
    {
        return parent::_prepareLayout();
    }

    public function getShout()
    {
        return 'Shouting to the world';
    }

    public function getLogo()
    {
//        return 'Assaka_Interceptor::images/primolens_logo.png';
        return 'images/primolens_logo.png';
    }

}